Name:           uacme
Version:        1.7.1
Release:        1%{?dist}
Summary:        ACMEv2 client written in plain C with minimal dependencies 
URL:            https://github.com/ndilieto/uacme
License:        GPLv3

Source0: https://github.com/ndilieto/%{name}/archive/upstream/%{version}/%{name}-%{version}.tar.gz

Requires:       libcurl gnutls libev
BuildRequires:  gcc libcurl-devel gnutls-devel libev-devel asciidoc make

%description
ACMEv2 client written in plain C with minimal dependencies and
lightweight proxying tls-alpn-01 challenge responder, designed
to handle incoming HTTPS connections on port 443.

The ACMEv2 protocol allows a Certificate Authority and an
applicant to automate the process of verification and certificate
issuance. The protocol also provides facilities for other
certificate management functions, such as certificate revocation.

%prep
%autosetup -n %{name}-upstream-%{version}

%build
%configure --disable-maintainer-mode --with-gnutls
make %{?_smp_mflags}

%check

%install
%make_install
install -Dm 644 COPYING -t $RPM_BUILD_ROOT%{_docdir}/%{name}/

%files
%{_bindir}/%{name}
%{_bindir}/ualpn
%{_mandir}/man1/%{name}.1.gz
%{_mandir}/man1/ualpn.1.gz
%{_datadir}/%{name}
%license %{_docdir}/%{name}/COPYING
%doc %{_docdir}/%{name}/%{name}.html
%doc %{_docdir}/%{name}/ualpn.html

%changelog
* Mon Jan 31 2022 Hugo Rodrigues <hugorodrigues@hugorodrigues.zyz>
- Use GNUTls and bundle uacme and ualpn on the same package

* Fri Jan 28 2022 Hugo Rodrigues <hugorodrigues@hugorodrigues.xyz>
- Initial RPM package
