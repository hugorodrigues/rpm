%define github_owner acmesh-official
%define github_repo  acme.sh

Name:           acme.sh
Version:        3.0.4
Release:        1%{?dist}
Summary:        An ACME Shell script, an acme client alternative to certbot
URL:            https://github.com/acmesh-official/acme.sh
BuildArch:      noarch
License:        GPLv3

Source0: https://github.com/%{github_owner}/%{github_repo}/archive/%{version}/%{github_repo}-%{version}.tar.gz
Source1: acmesh.service
Source2: acmesh.timer

Requires:       curl socat openssl systemd
BuildRequires:  systemd-rpm-macros

%description
A pure Unix shell script implementing ACME client protocol

%prep
%autosetup -n %{github_repo}-%{version}

# replace macros in source files
sed -i -e "s|%%{_bindir}|%{_bindir}|g" \
       -e "s|%%{_sbindir}|%{_sbindir}|g" \
       -e "s|%%{_libexecdir}|%{_libexecdir}|g" \
       -e "s|%%{_sysconfdir}|%{_sysconfdir}|g" \
       -e "s|%%{_initddir}|%{_initddir}|g" \
       -e "s|%%{_localstatedir}|%{_localstatedir}|g" \
       -e "s|%%{_sharedstatedir}|%{_sharedstatedir}|g" \
       -e "s|%%{_rundir}|%{_rundir}|g" \
       -e "s|%%{_pkgdocdir}|%{_pkgdocdir}|g" \
       -e "s|%%{_datadir}|%{_datadir}|g" \
       -e "s|%%{name}|%{name}|g" \
       -e "s|%%{binname}|%{binname}|g" \
       %{SOURCE1} %{SOURCE2}

%build

%install
install -Dm 755 acme.sh -t $RPM_BUILD_ROOT%{_datadir}/%{name}
install -Dm 755 deploy/*.sh -t $RPM_BUILD_ROOT%{_datadir}/%{name}/deploy/
install -Dm 755 dnsapi/*.sh -t $RPM_BUILD_ROOT%{_datadir}/%{name}/dnsapi/
install -Dm 755 notify/*.sh -t $RPM_BUILD_ROOT%{_datadir}/%{name}/notify/

install -Dm 644 README.md -t $RPM_BUILD_ROOT%{_docdir}/%{name}/
install -Dm 644 LICENSE.md -t $RPM_BUILD_ROOT%{_docdir}/%{name}/

install -m 755 -d $RPM_BUILD_ROOT%{_bindir}

install -Dm 644 %{SOURCE1} -t $RPM_BUILD_ROOT%{_userunitdir}
install -Dm 644 %{SOURCE2} -t $RPM_BUILD_ROOT%{_userunitdir}

ln -s %{_datadir}/%{name}/acme.sh $RPM_BUILD_ROOT%{_bindir}/acme.sh

%files
%license %{_docdir}/%{name}/LICENSE.md
%doc %{_docdir}/%{name}/README.md
%{_datadir}/%{name}/
%{_bindir}/acme.sh
%{_userunitdir}/acmesh.service
%{_userunitdir}/acmesh.timer

%changelog
* Wed May 12 2022 Hugo Rodrigues (hugorodrigues@hugorodrigues.xyz)
- Upgraded to 3.0.4

* Mon Dec 6 2021 Hugo Rodrigues <hugorodrigues@hugorodrigues.xyz>
- Added systemD services

* Fri Dec 3 2021 Hugo Rodrigues <hugorodrigues@hugorodrigues.xyz>
- Initial RPM package
